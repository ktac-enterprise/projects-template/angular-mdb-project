# angular-mdb-project

Template de projet Angular, configuré pour être accessible à parti de n'importe quel hôte, et comportant une configuration Matérial Design de Bootstrap.

Ce projet a été généré avec [Angular CLI](https://github.com/angular/angular-cli) version 10.2.0.

## serveur de Développement

Executez `ng serve` pour un serveur de développement. Allez à `http://localhost:4200/`. L'Application va automatique s'actualiser si vous changez n'importe quel fichier source.

## Installation des Module

Executez `npm install` pour installer tous les modules qui on été utiliser dans ce projet, pour qu'ils puissent être disponible dans votre dossier node-module local.


## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
